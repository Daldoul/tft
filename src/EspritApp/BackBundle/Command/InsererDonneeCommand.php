<?php

namespace EspritApp\BackBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use EspritApp\BackBundle\Entity\Utilisateur;

// pour tester : $  php app/console  inserer:donnee -- admin
class InsererDonneeCommand extends ContainerAwareCommand {

    protected function configure() {
        $this
                ->setName('inserer:donnee')
//                ->setDescription('the current user will not be able to connect,still available but locked')
                ->addArgument('name', InputArgument::OPTIONAL, 'Who do you want to greet?')
                ->addOption(
                        'yell', null, InputOption::VALUE_NONE, 'If set, the task will yell in uppercase letters'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {


        $role = $input->getArgument('name');


        try {
            $resultat = $this->InsererDonnee($role);
        } catch (\Exception $e) {
            $resultat = FALSE;
        }


        if ($resultat == FALSE) {
            $output->writeln('not done ');
        } else {
            $output->writeln('done');
        }
    }

    public function InsererDonnee($role) {

        $em = $this->getContainer()->get('doctrine')->getManager();
        $role = strtolower($role);
        try {
            $user = new Utilisateur();
            $user->setUsername($role);
            $user->setEmail($role . "@esprit.tn");
            $user->setPlainPassword($role);
            $user->setPrenom($role . "prenom");
            $user->setNom($role . "nom");
            $user->setCin(0123456789);
            $user->setTelephone(25112990);
            $user->setAdresse("ici");
            $user->setEnabled(true);
            $user->setNewsletter(false);

            if ($role == 'admin') {
                $user->setRoles(array('ROLE_ADMIN' => 'ROLE_ADMIN'));
            }
            if ($role == 'arbitre') {
                $user->setRoles(array('ROLE_ARBITRE' => 'ROLE_ARBITRE'));
            }
            if ($role == 'medecin') {
                $user->setRoles(array('ROLE_MEDECIN' => 'ROLE_MEDECIN'));
            }
            if ($role == 'joueur') {
                $user->setRoles(array('ROLE_JOUEUR' => 'ROLE_JOUEUR'));
            }
            if ($role == 'responsable') {
                $user->setRoles(array('ROLE_RESPONSABLE' => 'ROLE_RESPONSABLE'));
            }
            if ($role == 'client') {
                $user->setRoles(array('ROLE_CLIENT' => 'ROLE_CLIENT'));
            }

            $em->persist($user);
            $em->flush();
            $resultat = TRUE;
        } catch (\Exception $e) {
            $resultat = FALSE;
        }
        return $resultat;
    }
}
