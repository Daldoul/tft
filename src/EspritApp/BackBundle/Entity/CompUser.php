<?php

namespace EspritApp\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CompUser
 */
class CompUser
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \EspritApp\BackBundle\Entity\Utilisateur
     */
    private $idUser;

    /**
     * @var \EspritApp\BackBundle\Entity\Competition
     */
    private $idCompetition;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUser
     *
     * @param \EspritApp\BackBundle\Entity\Utilisateur $idUser
     * @return CompUser
     */
    public function setIdUser(\EspritApp\BackBundle\Entity\Utilisateur $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \EspritApp\BackBundle\Entity\Utilisateur 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idCompetition
     *
     * @param \EspritApp\BackBundle\Entity\Competition $idCompetition
     * @return CompUser
     */
    public function setIdCompetition(\EspritApp\BackBundle\Entity\Competition $idCompetition = null)
    {
        $this->idCompetition = $idCompetition;

        return $this;
    }

    /**
     * Get idCompetition
     *
     * @return \EspritApp\BackBundle\Entity\Competition 
     */
    public function getIdCompetition()
    {
        return $this->idCompetition;
    }
}
