<?php

namespace EspritApp\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MatcheUser
 */
class MatcheUser {
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \EspritApp\BackBundle\Entity\Matche
     */
    private $idMatche;

    /**
     * @var \EspritApp\BackBundle\Entity\Utilisateur
     */
    private $idUser;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idMatche
     *
     * @param \EspritApp\BackBundle\Entity\Matche $idMatche
     * @return MatcheUser
     */
    public function setIdMatche(\EspritApp\BackBundle\Entity\Matche $idMatche = null) {
        $this->idMatche = $idMatche;

        return $this;
    }

    /**
     * Get idMatche
     *
     * @return \EspritApp\BackBundle\Entity\Matche
     */
    public function getIdMatche() {
        return $this->idMatche;
    }

    /**
     * Set idUser
     *
     * @param \EspritApp\BackBundle\Entity\Utilisateur $idUser
     * @return MatcheUser
     */
    public function setIdUser(\EspritApp\BackBundle\Entity\Utilisateur $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \EspritApp\BackBundle\Entity\Utilisateur 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
