<?php

namespace EspritApp\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TestUser
 */
class TestUser
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \EspritApp\BackBundle\Entity\Test
     */
    private $idTest;

    /**
     * @var \EspritApp\BackBundle\Entity\Utilisateur
     */
    private $idUser;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTest
     *
     * @param \EspritApp\BackBundle\Entity\Test $idTest
     * @return TestUser
     */
    public function setIdTest(\EspritApp\BackBundle\Entity\Test $idTest = null)
    {
        $this->idTest = $idTest;

        return $this;
    }

    /**
     * Get idTest
     *
     * @return \EspritApp\BackBundle\Entity\Test 
     */
    public function getIdTest()
    {
        return $this->idTest;
    }

    /**
     * Set idUser
     *
     * @param \EspritApp\BackBundle\Entity\Utilisateur $idUser
     * @return TestUser
     */
    public function setIdUser(\EspritApp\BackBundle\Entity\Utilisateur $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \EspritApp\BackBundle\Entity\Utilisateur 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
