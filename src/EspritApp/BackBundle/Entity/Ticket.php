<?php

namespace EspritApp\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ticket
 */
class Ticket
{
    /**
     * @var integer
     */
    private $prix;
    /**
     * @var integer
     */
    private $quantite;
    /**
     * @var string
     */
    private $categorie;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \EspritApp\BackBundle\Entity\Utilisateur
     */
    private $idUser;

    /**
     * @var \EspritApp\BackBundle\Entity\Matche
     */
    private $idMatche;

    /**
     * Set prix
     *
     * @param integer $prix
     * @return Ticket
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return integer 
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set categorie
     *
     * @param string $categorie
     * @return Ticket
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return string 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUser
     *
     * @param \EspritApp\BackBundle\Entity\Utilisateur $idUser
     * @return Ticket
     */
    public function setIdUser(\EspritApp\BackBundle\Entity\Utilisateur $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \EspritApp\BackBundle\Entity\Utilisateur 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idMatche
     *
     * @param \EspritApp\BackBundle\Entity\Matche $idMatche
     * @return Ticket
     */
    public function setIdMatche(\EspritApp\BackBundle\Entity\Matche $idMatche = null) {
        $this->idMatche = $idMatche;

        return $this;
    }

    /**
     * Get idMatche
     *
     * @return \EspritApp\BackBundle\Entity\Matche
     */
    public function getIdMatche() {
        return $this->idMatche;
    }
    function getQuantite() {
        return $this->quantite;
    }

    function setQuantite($quantite) {
        $this->quantite = $quantite;
    }


}
