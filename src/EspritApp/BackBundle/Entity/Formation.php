<?php

namespace EspritApp\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Formation
 */
class Formation {

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $lieux;

    /**
     * @var \DateTime
     */
    private $dateOuverture;

    /**
     * @var \DateTime
     */
    private $dateClouture;

    /**
     * @var string
     */
    private $type;

    /**
     * @var integer
     */
    private $id;

    /**
     * Set nom
     *
     * @param string $nom
     * @return Formation
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set lieux
     *
     * @param string $lieux
     * @return Formation
     */
    public function setLieux($lieux) {
        $this->lieux = $lieux;

        return $this;
    }

    /**
     * Get lieux
     *
     * @return string 
     */
    public function getLieux() {
        return $this->lieux;
    }

    /**
     * Set dateOuverture
     *
     * @param \DateTime $dateOuverture
     * @return Formation
     */
    public function setDateOuverture($dateOuverture) {
        $this->dateOuverture = $dateOuverture;

        return $this;
    }

    /**
     * Get dateOuverture
     *
     * @return \DateTime 
     */
    public function getDateOuverture() {
        return $this->dateOuverture;
    }

    /**
     * Set dateClouture
     *
     * @param \DateTime $dateClouture
     * @return Formation
     */
    public function setDateClouture($dateClouture) {
        $this->dateClouture = $dateClouture;

        return $this;
    }

    /**
     * Get dateClouture
     *
     * @return \DateTime 
     */
    public function getDateClouture() {
        return $this->dateClouture;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Formation
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    public function __toString() {
        return $this->nom . '';
    }

}
