<?php

namespace EspritApp\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Matche
 */
class Matche {
    /**
     * @var string
     */
    private $lieux;

    /**
     * @var string
     */
    private $type;

    /**
     * @var \DateTime
     */
    private $dateDebut;

    /**
     * @var \DateTime
     */
    private $dateFin;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \EspritApp\BackBundle\Entity\Competition
     */
    private $idCompetition;

    /**
     * @var \EspritApp\BackBundle\Entity\Evenement
     */
    private $idEvent;


    /**
     * Set lieux
     *
     * @param string $lieux
     * @return Matche
     */
    public function setLieux($lieux)
    {
        $this->lieux = $lieux;

        return $this;
    }

    /**
     * Get lieux
     *
     * @return string 
     */
    public function getLieux()
    {
        return $this->lieux;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Matche
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     * @return Matche
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime 
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     * @return Matche
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime 
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCompetition
     *
     * @param \EspritApp\BackBundle\Entity\Competition $idCompetition
     * @return Matche
     */
    public function setIdCompetition(\EspritApp\BackBundle\Entity\Competition $idCompetition = null)
    {
        $this->idCompetition = $idCompetition;

        return $this;
    }

    /**
     * Get idCompetition
     *
     * @return \EspritApp\BackBundle\Entity\Competition 
     */
    public function getIdCompetition()
    {
        return $this->idCompetition;
    }

    /**
     * Set idEvent
     *
     * @param \EspritApp\BackBundle\Entity\Evenement $idEvent
     * @return Matche
     */
    public function setIdEvent(\EspritApp\BackBundle\Entity\Evenement $idEvent = null)
    {
        $this->idEvent = $idEvent;

        return $this;
    }

    /**
     * Get idEvent
     *
     * @return \EspritApp\BackBundle\Entity\Evenement 
     */
    public function getIdEvent()
    {
        return $this->idEvent;
    }
}
