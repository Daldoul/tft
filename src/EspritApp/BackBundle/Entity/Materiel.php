<?php

namespace EspritApp\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Materiel
 */
class Materiel
{
    /**
     * @var string
     */
    private $nom;

    /**
     * @var integer
     */
    private $quantite;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \EspritApp\BackBundle\Entity\Utilisateur
     */
    private $idUser;


    /**
     * Set nom
     *
     * @param string $nom
     * @return Materiel
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set quantite
     *
     * @param integer $quantite
     * @return Materiel
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer 
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUser
     *
     * @param \EspritApp\BackBundle\Entity\Utilisateur $idUser
     * @return Materiel
     */
    public function setIdUser(\EspritApp\BackBundle\Entity\Utilisateur $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \EspritApp\BackBundle\Entity\Utilisateur 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
