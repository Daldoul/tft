<?php

namespace EspritApp\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FormaUser
 */
class FormaUser
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \EspritApp\BackBundle\Entity\Formation
     */
    private $idFormation;

    /**
     * @var \EspritApp\BackBundle\Entity\Utilisateur
     */
    private $idUser;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idFormation
     *
     * @param \EspritApp\BackBundle\Entity\Formation $idFormation
     * @return FormaUser
     */
    public function setIdFormation(\EspritApp\BackBundle\Entity\Formation $idFormation = null)
    {
        $this->idFormation = $idFormation;

        return $this;
    }

    /**
     * Get idFormation
     *
     * @return \EspritApp\BackBundle\Entity\Formation 
     */
    public function getIdFormation()
    {
        return $this->idFormation;
    }

    /**
     * Set idUser
     *
     * @param \EspritApp\BackBundle\Entity\Utilisateur $idUser
     * @return FormaUser
     */
    public function setIdUser(\EspritApp\BackBundle\Entity\Utilisateur $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \EspritApp\BackBundle\Entity\Utilisateur 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }
}
