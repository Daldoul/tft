<?php

namespace EspritApp\BackBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventUser
 */
class EventUser
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \EspritApp\BackBundle\Entity\Utilisateur
     */
    private $idUser;

    /**
     * @var \EspritApp\BackBundle\Entity\Evenement
     */
    private $idEvent;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUser
     *
     * @param \EspritApp\BackBundle\Entity\Utilisateur $idUser
     * @return EventUser
     */
    public function setIdUser(\EspritApp\BackBundle\Entity\Utilisateur $idUser = null)
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * Get idUser
     *
     * @return \EspritApp\BackBundle\Entity\Utilisateur 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * Set idEvent
     *
     * @param \EspritApp\BackBundle\Entity\Evenement $idEvent
     * @return EventUser
     */
    public function setIdEvent(\EspritApp\BackBundle\Entity\Evenement $idEvent = null)
    {
        $this->idEvent = $idEvent;

        return $this;
    }

    /**
     * Get idEvent
     *
     * @return \EspritApp\BackBundle\Entity\Evenement 
     */
    public function getIdEvent()
    {
        return $this->idEvent;
    }
}
