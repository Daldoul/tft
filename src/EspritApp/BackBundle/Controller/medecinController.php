<?php

namespace EspritApp\BackBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EspritApp\BackBundle\Entity\Utilisateur;
use EspritApp\BackBundle\Form\UserType;

class medecinController extends Controller {

    public function addAction() {
        $medecin = new Utilisateur();
        $form = $this->createForm(new UserType, $medecin);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $medecin = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $medecin->setRoles(array('ROLE_MEDECIN' => 'ROLE_MEDECIN'));
                $medecin->setEnabled(true);
                $em->persist($medecin);
                $em->flush();
                return $this->redirect($this->generateUrl('medecin_show'));
            }
        }
        return $this->render('EspritAppBackBundle:medecins:add.html.twig', array('form' => $form->createView()));
    }

    public function updateAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $medecin = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('id' => $id));
        if (!$medecin) {
            throw $this->createNotFoundException('no  medecins found');
        }   
        if (!in_array('ROLE_MEDECIN', $medecin->getRoles())) {
            throw $this->createNotFoundException('No medecins found for id ' . $id);
        }
        $form = $this->createForm(new UserType, $medecin);
        if ($request->isMethod('Post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $medecin = $form->getData(); 
                $medecin->setRoles(array('ROLE_MEDECIN' => 'ROLE_MEDECIN'));
                $medecin->setEnabled(true);
                $em->persist($medecin);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été modifiées avec succés!');
                return $this->redirect($this->generateUrl('medecin_update', array('id' => $id)));
            }
        }
        return $this->render('EspritAppBackBundle:medecins:update.html.twig', array('form' => $form->createView(), 'id' => $id));
    }

    public function showAction() {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('EspritAppBackBundle:Utilisateur')->findAll();
        $medecin= array();
        foreach ($users as $user) {
            if (in_array('ROLE_MEDECIN', $user->getRoles())) {
                $medecin[] = $user;
            }
        }
        return $this->render('EspritAppBackBundle:medecins:show.html.twig', array(
                    'medecin' => $medecin,
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $medecin = $em->getRepository('EspritAppBackBundle:Utilisateur')->find($id);
        if (!$medecin) {
            throw $this->createNotFoundException('No medecins found for id ' . $id);
        }
        if (!in_array('ROLE_MEDECIN', $medecin->getRoles())) {
            throw $this->createNotFoundException('No medecins found for id ' . $id);
        }
        $em->remove($medecin);
        $em->flush();
        return $this->redirect($this->generateUrl('medecin_show'));
    }

}
