<?php

namespace EspritApp\BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EspritApp\BackBundle\Entity\Stade;
use EspritApp\BackBundle\Form\StadeType;

class stadeController extends Controller {

    public function addAction() {
        $stade = new Stade();
        $form = $this->createForm(new StadeType, $stade);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $stade = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($stade);
                $em->flush();
                return $this->redirect($this->generateUrl('stade_show'));
            }
        }
        return $this->render('EspritAppBackBundle:stades:add.html.twig', array('form' => $form->createView()));
    }

    public function updateAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $stade = $em->getRepository('EspritAppBackBundle:Stade')->findOneBy(array('id' => $id));
        if (!$stade) {
            throw $this->createNotFoundException('no  Stade found');
        }

        $form = $this->createForm(new StadeType, $stade);
        if ($request->isMethod('Post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $stade = $form->getData();
                $em->persist($stade);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été modifiées avec succés!');
                return $this->redirect($this->generateUrl('stade_update', array('id' => $id)));
            }
        }
        return $this->render('EspritAppBackBundle:stades:update.html.twig', array('form' => $form->createView(), 'id' => $id));
    }

    public function showAction() {
        $em = $this->getDoctrine()->getManager();
        $stades = $em->getRepository('EspritAppBackBundle:Stade')->findAll();
        return $this->render('EspritAppBackBundle:stades:show.html.twig', array(
                    'stades' => $stades,
        ));
    }

    public function showfrontAction(Request $request) {
        
        $em = $this->getDoctrine()->getManager();
        $stades = $em->getRepository('EspritAppBackBundle:Stade')->findAll();
        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }

        return $this->render('EspritAppFrontBundle:Stades:showfront.html.twig', array(
                    'stades' => $stades, 'formNewsLetters' => $formNewsLetters->createView()
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $stade = $em->getRepository('EspritAppBackBundle:Stade')->find($id);
        if (!$stade) {
            throw $this->createNotFoundException('No Stade found for id ' . $id);
        }
        $em->remove($stade);
        $em->flush();
        return $this->redirect($this->generateUrl('stade_show'));
    }

}
