<?php

namespace EspritApp\BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EspritApp\BackBundle\Entity\Club;
use EspritApp\BackBundle\Form\ClubType;

class clubController extends Controller {

    public function addAction() {
        $club = new Club();
        $form = $this->createForm(new ClubType, $club);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $club = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($club);
                $em->flush();
                return $this->redirect($this->generateUrl('club_show'));
            }
        }
        return $this->render('EspritAppBackBundle:clubs:add.html.twig', array('form' => $form->createView()));
    }

    public function updateAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $club = $em->getRepository('EspritAppBackBundle:Club')->findOneBy(array('id' => $id));
        if (!$club) {
            throw $this->createNotFoundException('no  Club found');
        }

        $form = $this->createForm(new ClubType, $club);
        if ($request->isMethod('Post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $club = $form->getData();
                $em->persist($club);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été modifiées avec succés!');
                return $this->redirect($this->generateUrl('club_update', array('id' => $id)));
            }
        }
        return $this->render('EspritAppBackBundle:clubs:update.html.twig', array('form' => $form->createView(), 'id' => $id));
    }

    public function showAction() {
        $em = $this->getDoctrine()->getManager();
        $clubs = $em->getRepository('EspritAppBackBundle:Club')->findAll();
        return $this->render('EspritAppBackBundle:clubs:show.html.twig', array(
                    'club' => $clubs,
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $club = $em->getRepository('EspritAppBackBundle:Club')->find($id);
        if (!$club) {
            throw $this->createNotFoundException('No Club found for id ' . $id);
        }
        $em->remove($club);
        $em->flush();
        return $this->redirect($this->generateUrl('club_show'));
    }

}
