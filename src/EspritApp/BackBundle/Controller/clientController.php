<?php

namespace EspritApp\BackBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EspritApp\BackBundle\Entity\Utilisateur;
use EspritApp\BackBundle\Form\UserType;

class clientController extends Controller {

    public function addAction() {
        $client = new Utilisateur();
        $form = $this->createForm(new UserType, $client);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $client = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $client->setRoles(array('ROLE_CLIENT' => 'ROLE_CLIENT'));
                $client->setEnabled(true);
                $em->persist($client);
                $em->flush();
                return $this->redirect($this->generateUrl('client_show'));
            }
        }
        return $this->render('EspritAppBackBundle:clients:add.html.twig', array('form' => $form->createView()));
    }

    public function updateAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('id' => $id));
        if (!$client) {
            throw $this->createNotFoundException('no  clients found');
        }   
        if (!in_array('ROLE_CLIENT', $client->getRoles())) {
            throw $this->createNotFoundException('No clients found for id ' . $id);
        }
        $form = $this->createForm(new UserType, $client);
        if ($request->isMethod('Post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $client = $form->getData(); 
                $client->setRoles(array('ROLE_CLIENT' => 'ROLE_CLIENT'));
                $client->setEnabled(true);
                $em->persist($client);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été modifiées avec succés!');
                return $this->redirect($this->generateUrl('client_update', array('id' => $id)));
            }
        }
        return $this->render('EspritAppBackBundle:clients:update.html.twig', array('form' => $form->createView(), 'id' => $id));
    }

    public function showAction() {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('EspritAppBackBundle:Utilisateur')->findAll();
        $client= array();
        foreach ($users as $user) {
            if (in_array('ROLE_CLIENT', $user->getRoles())) {
                $client[] = $user;
            }
        }
        return $this->render('EspritAppBackBundle:clients:show.html.twig', array(
                    'client' => $client,
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository('EspritAppBackBundle:Utilisateur')->find($id);
        if (!$client) {
            throw $this->createNotFoundException('No clients found for id ' . $id);
        }
        if (!in_array('ROLE_CLIENT', $client->getRoles())) {
            throw $this->createNotFoundException('No clients found for id ' . $id);
        }
        $em->remove($client);
        $em->flush();
        return $this->redirect($this->generateUrl('client_show'));
    }

}
