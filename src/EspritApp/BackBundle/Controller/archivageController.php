<?php

namespace EspritApp\BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;



class archivageController extends Controller {

    public function archivagejoueurAction() {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('EspritAppBackBundle:Utilisateur')->findAll();
        $joueur = array();
        foreach ($users as $user) {
            if (in_array('ROLE_JOUEUR', $user->getRoles())) {
                $joueur[] = $user;
            }
        }
        $pdf = new \FPDF();
        $titre = 'Archivages des Joueurs';
        $pdf->SetTitle($titre);
        $pdf->SetAuthor('Meriem');

        $numeroChapitre = 1;
        foreach ($joueur as $key => $value) {
            if ($value->getIdClub()) {
                $nomClub = $value->getIdClub()->getNom();
            } else {
                $nomClub = "sans club";
            }
            $contenue = "Nom : " . $value->getNom() . "\n" .
                    "Prenom : " . $value->getPrenom() . "\n" .
                    "Tranche d'age : " . $value->getTrancheage() . "\n" .
                    "Adresse : " . $value->getAdresse() . "\n" .
                    "Club : " . $nomClub . "\n"
            ;
            $this->AjouterChapitre($numeroChapitre, $value->getUsername(),"Joueur", $contenue, $pdf);
            $numeroChapitre = $numeroChapitre + 1;

        }
        $pdf->Output();
        exit;
    }

    
     public function archivagearbitreAction() {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('EspritAppBackBundle:Utilisateur')->findAll();
        $arbitre = array();
        foreach ($users as $user) {
            if (in_array('ROLE_ARBITRE', $user->getRoles())) {
                $arbitre[] = $user;
            }
        }
        $pdf = new \FPDF();
        $titre = 'Archivages des Arbitres';
        $pdf->SetTitle($titre);
        $pdf->SetAuthor('Meriem');

        $numeroChapitre = 1;
        foreach ($arbitre as $key => $value) {
           
                $nbmatch = count($em->getRepository('EspritAppBackBundle:MatcheUser')->findBy(array('idUser' => $value->getId())));
            
            $contenue = "Nom : " . $value->getNom() . "\n" . "\n" .
                    "Prenom : " . $value->getPrenom() . "\n" . "\n" .
                    "Adresse : " . $value->getAdresse() . "\n" . "\n" .
                    "Nombre de ses matchs : " . $nbmatch . "\n". "\n" 
            ;
            $this->AjouterChapitre($numeroChapitre, $value->getUsername(),"Arbitre", $contenue, $pdf);
            $numeroChapitre = $numeroChapitre + 1;

        }
        $pdf->Output();
        exit;
    }
 public function archivagematchesAction() {
        $em = $this->getDoctrine()->getManager();
        $Matches = $em->getRepository('EspritAppBackBundle:Matche')->findAll();
       
        $pdf = new \FPDF();
        $titre = 'Archivages des Matches';
        $pdf->SetTitle($titre);
        $pdf->SetAuthor('Meriem');

        $numeroChapitre = 1;
        foreach ($Matches as $key => $value) {
         
            $contenue = "Debut : " . $value->getDateDebut()->format('Y-m-d-H-i-s') . "\n" . "\n" .
                    "Fin : " . $value->getDateFin()->format('Y-m-d-H-i-s') . "\n" . "\n" .
                    "Lieu : " . $value->getLieux() . "\n" . "\n" .
                    "Type : " . $value->getType() . "\n" . "\n" .
                    "Evennement : " . $value->getIdEvent()->getNom() . "\n". "\n".
                     "Competition : " . $value->getIdCompetition()->getNom() . "\n". "\n"
            ;
            $this->AjouterChapitre($numeroChapitre, "Match ","Matche", $contenue, $pdf);
            $numeroChapitre = $numeroChapitre + 1;

        }
        $pdf->Output();
        exit;
    }

    
    
    
    
    
    
    
    
    
    protected function Header($pdf) {
        global $titre;

        // Arial gras 15
        $pdf->SetFont('Arial', 'B', 15);
        // Calcul de la largeur du titre et positionnement
        $w = $pdf->GetStringWidth($titre) + 6;
        $pdf->SetX((210 - $w) / 2);
        // Couleurs du cadre, du fond et du texte
        $pdf->SetDrawColor(0, 80, 180);
        $pdf->SetFillColor(230, 230, 0);
        $pdf->SetTextColor(220, 50, 50);
        // Epaisseur du cadre (1 mm)
        $pdf->SetLineWidth(1);
        // Titre
        $pdf->Cell($w, 9, $titre, 1, 1, 'C', true);
        // Saut de ligne
        $pdf->Ln(10);
    }

    protected function Footer($pdf) {
        // Positionnement à 1,5 cm du bas
        $pdf->SetY(-15);
        // Arial italique 8
        $pdf->SetFont('Arial', 'I', 8);
        // Couleur du texte en gris
        $pdf->SetTextColor(128);
        // Numéro de page
        $pdf->Cell(0, 10, 'Page ' . $pdf->PageNo(), 0, 0, 'C');
    }

    protected function TitreChapitre($num, $libelle,$typeCont, $pdf) {
        // Arial 12
        $pdf->SetFont('Arial', '', 12);
        // Couleur de fond
        $pdf->SetFillColor(200, 220, 255);
        // Titre
        $pdf->Cell(0, 6, $typeCont." N $num : $libelle", 0, 1, 'L', true);
        // Saut de ligne
        $pdf->Ln(4);
    }

    protected function CorpsChapitre($fichier, $pdf) {
        // Lecture du fichier texte
        $txt = $fichier;
        // Times 12
        $pdf->SetFont('Times', '', 12);
        // Sortie du texte justifié
        $pdf->MultiCell(0, 5, $txt);
        // Saut de ligne
        $pdf->Ln();
        // Mention en italique
        $pdf->SetFont('', 'I');
        $pdf->Cell(0, 5, "(fin ..)");
    }

    protected function AjouterChapitre($num, $libelle,$typeCont, $fichier, $pdf) {
        $pdf->AddPage(); 
        $this->TitreChapitre($num, $libelle, $typeCont,$pdf);
        $this->CorpsChapitre($fichier, $pdf);
    }

}
