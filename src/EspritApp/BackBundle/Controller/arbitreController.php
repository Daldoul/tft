<?php

namespace EspritApp\BackBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EspritApp\BackBundle\Entity\Utilisateur;
use EspritApp\BackBundle\Form\UserType;

class arbitreController extends Controller {

    public function addAction() {
        $arbitre = new Utilisateur();
        $form = $this->createForm(new UserType, $arbitre);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $arbitre = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $arbitre->setRoles(array('ROLE_ARBITRE' => 'ROLE_ARBITRE'));
                $arbitre->setEnabled(true);
                $em->persist($arbitre);
                $em->flush();
                return $this->redirect($this->generateUrl('arbitre_show'));
            }
        }
        return $this->render('EspritAppBackBundle:arbitres:add.html.twig', array('form' => $form->createView()));
    }

    public function updateAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $arbitre = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('id' => $id));
        if (!$arbitre) {
            throw $this->createNotFoundException('no  arbitres found');
        }   
        if (!in_array('ROLE_ARBITRE', $arbitre->getRoles())) {
            throw $this->createNotFoundException('No arbitres found for id ' . $id);
        }
        $form = $this->createForm(new UserType, $arbitre);
        if ($request->isMethod('Post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $arbitre = $form->getData(); 
                $arbitre->setRoles(array('ROLE_ARBITRE' => 'ROLE_ARBITRE'));
                $arbitre->setEnabled(true);
                $em->persist($arbitre);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été modifiées avec succés!');
                return $this->redirect($this->generateUrl('arbitre_update', array('id' => $id)));
            }
        }
        return $this->render('EspritAppBackBundle:arbitres:update.html.twig', array('form' => $form->createView(), 'id' => $id));
    }

    public function showAction() {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('EspritAppBackBundle:Utilisateur')->findAll();
        $arbitre= array();
        foreach ($users as $user) {
            if (in_array('ROLE_ARBITRE', $user->getRoles())) {
                $arbitre[] = $user;
            }
        }
        return $this->render('EspritAppBackBundle:arbitres:show.html.twig', array(
                    'arbitre' => $arbitre,
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $arbitre = $em->getRepository('EspritAppBackBundle:Utilisateur')->find($id);
        if (!$arbitre) {
            throw $this->createNotFoundException('No arbitres found for id ' . $id);
        }
        if (!in_array('ROLE_ARBITRE', $arbitre->getRoles())) {
            throw $this->createNotFoundException('No arbitres found for id ' . $id);
        }
        $em->remove($arbitre);
        $em->flush();
        return $this->redirect($this->generateUrl('arbitre_show'));
    }

}
