<?php

namespace EspritApp\BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EspritApp\BackBundle\Entity\Materiel;
use EspritApp\BackBundle\Form\MaterielType;

class materielController extends Controller {

    public function addAction() {
        $materiel = new Materiel();
        $form = $this->createForm(new MaterielType, $materiel);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $materiel = $form->getData();
                $em = $this->getDoctrine()->getManager();
                if ($this->getUser()) {
                    $materiel->setIdUser($this->getUser());
                }
                $em->persist($materiel);
                $em->flush();
                return $this->redirect($this->generateUrl('materiel_show'));
            }
        }
        return $this->render('EspritAppBackBundle:materiels:add.html.twig', array('form' => $form->createView()));
    }

    public function updateAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $materiel = $em->getRepository('EspritAppBackBundle:Materiel')->findOneBy(array('id' => $id));
        if (!$materiel) {
            throw $this->createNotFoundException('no  Materiel found');
        }

        $form = $this->createForm(new MaterielType, $materiel);
        if ($request->isMethod('Post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $materiel = $form->getData();

                if ($this->getUser()) {
                    $materiel->setIdUser($this->getUser());
                }

                $em->persist($materiel);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été modifiées avec succés!');
                return $this->redirect($this->generateUrl('materiel_update', array('id' => $id)));
            }
        }
        return $this->render('EspritAppBackBundle:materiels:update.html.twig', array('form' => $form->createView(), 'id' => $id));
    }

    public function showAction() {
        $em = $this->getDoctrine()->getManager();
        $materiels = $em->getRepository('EspritAppBackBundle:Materiel')->findAll();
        return $this->render('EspritAppBackBundle:materiels:show.html.twig', array(
                    'materiel' => $materiels,
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $materiel = $em->getRepository('EspritAppBackBundle:Materiel')->find($id);
        if (!$materiel) {
            throw $this->createNotFoundException('No Materiel found for id ' . $id);
        }
        $em->remove($materiel);
        $em->flush();
        return $this->redirect($this->generateUrl('materiel_show'));
    }

}
