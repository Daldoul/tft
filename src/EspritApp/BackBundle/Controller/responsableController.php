<?php

namespace EspritApp\BackBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EspritApp\BackBundle\Entity\Utilisateur;
use EspritApp\BackBundle\Form\UserType;

class responsableController extends Controller {

    public function addAction() {
        $responsable = new Utilisateur();
        $form = $this->createForm(new UserType, $responsable);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $responsable = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $responsable->setRoles(array('ROLE_RESPONSABLE' => 'ROLE_RESPONSABLE'));
                $responsable->setEnabled(true);
                $em->persist($responsable);
                $em->flush();
                return $this->redirect($this->generateUrl('responsable_show'));
            }
        }
        return $this->render('EspritAppBackBundle:responsables:add.html.twig', array('form' => $form->createView()));
    }

    public function updateAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $responsable = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('id' => $id));
        if (!$responsable) {
            throw $this->createNotFoundException('no  responsables found');
        }   
        if (!in_array('ROLE_RESPONSABLE', $responsable->getRoles())) {
            throw $this->createNotFoundException('No responsables found for id ' . $id);
        }
        $form = $this->createForm(new UserType, $responsable);
        if ($request->isMethod('Post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $responsable = $form->getData(); 
                $responsable->setRoles(array('ROLE_RESPONSABLE' => 'ROLE_RESPONSABLE'));
                $responsable->setEnabled(true);
                $em->persist($responsable);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été modifiées avec succés!');
                return $this->redirect($this->generateUrl('responsable_update', array('id' => $id)));
            }
        }
        return $this->render('EspritAppBackBundle:responsables:update.html.twig', array('form' => $form->createView(), 'id' => $id));
    }

    public function showAction() { 
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('EspritAppBackBundle:Utilisateur')->findAll();
        $responsable= array();
        foreach ($users as $user) {
            if (in_array('ROLE_RESPONSABLE', $user->getRoles())) {
                $responsable[] = $user;
            }
        }
        return $this->render('EspritAppBackBundle:responsables:show.html.twig', array(
                    'responsable' => $responsable,
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $responsable = $em->getRepository('EspritAppBackBundle:Utilisateur')->find($id);
        if (!$responsable) {
            throw $this->createNotFoundException('No responsables found for id ' . $id);
        }
        if (!in_array('ROLE_RESPONSABLE', $responsable->getRoles())) {
            throw $this->createNotFoundException('No responsables found for id ' . $id);
        }
        $em->remove($responsable);
        $em->flush();
        return $this->redirect($this->generateUrl('responsable_show'));
    }

}
