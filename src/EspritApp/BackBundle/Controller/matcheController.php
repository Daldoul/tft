<?php

namespace EspritApp\BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EspritApp\BackBundle\Entity\Matche;
use EspritApp\BackBundle\Form\MatcheType;

class matcheController extends Controller {

   public function addAction() {
        $matche = new Matche();
        $form = $this->createForm(new MatcheType, $matche);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $matche = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($matche);
                $em->flush();
                return $this->redirect($this->generateUrl('matche_show'));
            }
        }
        return $this->render('EspritAppBackBundle:matches:add.html.twig', array('form' => $form->createView()));
    }

    public function updateAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $matche = $em->getRepository('EspritAppBackBundle:Matche')->findOneBy(array('id' => $id));
        if (!$matche) {
            throw $this->createNotFoundException('no  Matche found');
        }

        $form = $this->createForm(new MatcheType, $matche);
        if ($request->isMethod('Post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $matche = $form->getData();
                $em->persist($matche);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été modifiées avec succés!');
                return $this->redirect($this->generateUrl('matche_update', array('id' => $id)));
            }
        }
        return $this->render('EspritAppBackBundle:matches:update.html.twig', array('form' => $form->createView(), 'id' => $id));
    }

    public function showAction() {
        $em = $this->getDoctrine()->getManager();
        $matches = $em->getRepository('EspritAppBackBundle:Matche')->findAll();
        return $this->render('EspritAppBackBundle:matches:show.html.twig', array(
                    'matches' => $matches,
        ));
    }

    public function showfrontAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT a FROM EspritAppBackBundle:Matche a";
        $query = $em->createQuery($dql);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, /* query NOT result */ $request->query->getInt('page', 1)/* page number */, 2/* limit per page */
        );


        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }


        return $this->render('EspritAppFrontBundle:Matches:showfront.html.twig', array(
                    'pagination' => $pagination, 'formNewsLetters' => $formNewsLetters->createView()
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $matche = $em->getRepository('EspritAppBackBundle:Matche')->find($id);
        if (!$matche) {
            throw $this->createNotFoundException('No Matche found for id ' . $id);
        }
        $em->remove($matche);
        $em->flush();
        return $this->redirect($this->generateUrl('matche_show'));
    }

}
