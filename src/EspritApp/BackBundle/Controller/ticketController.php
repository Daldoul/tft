<?php

namespace EspritApp\BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EspritApp\BackBundle\Entity\Ticket;
use EspritApp\BackBundle\Form\TicketType;

class ticketController extends Controller {

    public function addAction() {
        $ticket = new Ticket();
        $form = $this->createForm(new TicketType, $ticket);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $ticket = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($ticket);
                $em->flush();
                return $this->redirect($this->generateUrl('ticket_show'));
            }
        }
        return $this->render('EspritAppBackBundle:tickets:add.html.twig', array('form' => $form->createView()));
    }

    public function updateAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $ticket = $em->getRepository('EspritAppBackBundle:Ticket')->findOneBy(array('id' => $id));
        if (!$ticket) {
            throw $this->createNotFoundException('no  Ticket found');
        }

        $form = $this->createForm(new TicketType, $ticket);
        if ($request->isMethod('Post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $ticket = $form->getData();
                $em->persist($ticket);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été modifiées avec succés!');
                return $this->redirect($this->generateUrl('ticket_update', array('id' => $id)));
            }
        }
        return $this->render('EspritAppBackBundle:tickets:update.html.twig', array('form' => $form->createView(), 'id' => $id));
    }

    public function showAction() {
        $em = $this->getDoctrine()->getManager();
        $tickets = $em->getRepository('EspritAppBackBundle:Ticket')->findAll();
        return $this->render('EspritAppBackBundle:tickets:show.html.twig', array(
                    'tickets' => $tickets,
        ));
    }

    public function showfrontAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $tickets = $em->getRepository('EspritAppBackBundle:Ticket')->findAll();
        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }
        return $this->render('EspritAppFrontBundle:Ticket:showfront.html.twig', array(
                    'tickets' => $tickets, 'formNewsLetters' => $formNewsLetters->createView()
        ));
    }

    public function achatTicketAction($id, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $ticketChoisi = $em->getRepository('EspritAppBackBundle:Ticket')->find($id);
        $ticketChoisi->setIdUser($user);
        $ticketChoisi->setQuantite(($ticketChoisi->getQuantite()-1));
        $em->persist($ticketChoisi);
        $em->flush();
        return $this->redirect($this->generateUrl('ticket_showfront'));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $ticket = $em->getRepository('EspritAppBackBundle:Ticket')->find($id);
        if (!$ticket) {
            throw $this->createNotFoundException('No Ticket found for id ' . $id);
        }
        $em->remove($ticket);
        $em->flush();
        return $this->redirect($this->generateUrl('ticket_show'));
    }

}
