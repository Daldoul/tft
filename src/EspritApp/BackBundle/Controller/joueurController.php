<?php
namespace EspritApp\BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EspritApp\BackBundle\Entity\Utilisateur;
use EspritApp\BackBundle\Form\UserType;

use EspritApp\BackBundle\Entity\FormaUser;
use EspritApp\BackBundle\Form\FormaUserType;
use EspritApp\BackBundle\Entity\MatcheUser;
use EspritApp\BackBundle\Form\MatcheUserType;
use EspritApp\BackBundle\Entity\CompUser;
use EspritApp\BackBundle\Form\CompUserType;
use EspritApp\BackBundle\Entity\EventUser;
use EspritApp\BackBundle\Form\EventUserType;

class joueurController extends Controller {

    public function addAction() {
        $joueur = new Utilisateur();
        $form = $this->createForm(new UserType, $joueur);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $joueur = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $joueur->setRoles(array('ROLE_JOUEUR' => 'ROLE_JOUEUR'));
                $joueur->setEnabled(true);
                $em->persist($joueur);
                $em->flush();
                return $this->redirect($this->generateUrl('joueur_show'));
            }
        }
        return $this->render('EspritAppBackBundle:joueurs:add.html.twig', array('form' => $form->createView()));
    }

    public function updateAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $joueur = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('id' => $id));
        if (!$joueur) {
            throw $this->createNotFoundException('no  joueurs found');
        }
        if (!in_array('ROLE_JOUEUR', $joueur->getRoles())) {
            throw $this->createNotFoundException('No joueurs found for id ' . $id);
        }
        $form = $this->createForm(new UserType, $joueur);
        if ($request->isMethod('Post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $joueur = $form->getData();
                $joueur->setRoles(array('ROLE_JOUEUR' => 'ROLE_JOUEUR'));
                $joueur->setEnabled(true);
                $em->persist($joueur);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été modifiées avec succés!');
                return $this->redirect($this->generateUrl('joueur_update', array('id' => $id)));
            }
        }
        return $this->render('EspritAppBackBundle:joueurs:update.html.twig', array('form' => $form->createView(), 'id' => $id));
    }

    public function showAction() {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('EspritAppBackBundle:Utilisateur')->findAll();
        $joueur = array();
        foreach ($users as $user) {
            if (in_array('ROLE_JOUEUR', $user->getRoles())) {
                $joueur[] = $user;
            }
        }
        return $this->render('EspritAppBackBundle:joueurs:show.html.twig', array(
                    'joueur' => $joueur,
        ));
    }

    public function showfrontAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('EspritAppBackBundle:Utilisateur')->findAll();
        $joueur = array();
        foreach ($users as $user) {
            if (in_array('ROLE_JOUEUR', $user->getRoles())) {
                $joueur[] = $user;
            }
        }
        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }
        return $this->render('EspritAppFrontBundle:Joueurs:showfront.html.twig', array(
                    'joueur' => $joueur, 'formNewsLetters' => $formNewsLetters->createView()
        ));
    }

    public function showfrontdetailAction($id, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $joueur = $em->getRepository('EspritAppBackBundle:Utilisateur')->find($id);
        $joueurs[] = $joueur;
        if (!$joueur) {
            throw $this->createNotFoundException('No joueurs found for id ' . $id);
        }
        if (!in_array('ROLE_JOUEUR', $joueur->getRoles())) {
            throw $this->createNotFoundException('No joueurs found for id ' . $id);
        }


        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }

        return $this->render('EspritAppFrontBundle:Joueurs:showfrontdetail.html.twig', array(
                    'joueur' => $joueurs, 'formNewsLetters' => $formNewsLetters->createView()
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $joueur = $em->getRepository('EspritAppBackBundle:Utilisateur')->find($id);
        if (!$joueur) {
            throw $this->createNotFoundException('No joueurs found for id ' . $id);
        }
        if (!in_array('ROLE_JOUEUR', $joueur->getRoles())) {
            throw $this->createNotFoundException('No joueurs found for id ' . $id);
        }
        $em->remove($joueur);
        $em->flush();
        return $this->redirect($this->generateUrl('joueur_show'));
    }

    public function affectationsToEventsAction() {
        $eventuser = new EventUser();
        $form = $this->createForm(new EventUserType, $eventuser);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $eventuser = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($eventuser);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été ajouté avec succés!');
                return $this->redirect($this->generateUrl('affectationsToEvents'));
            }
        }
        return $this->render('EspritAppBackBundle:affectationsjoueurs:affectationsToEvents.html.twig', array('form' => $form->createView()));
    }

    public function affectationsToMatchsAction() {
        $matchuser = new MatcheUser();
        $form = $this->createForm(new MatcheUserType, $matchuser);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $matchuser = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($matchuser);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été ajouté avec succés!');
                return $this->redirect($this->generateUrl('affectationsToMatchs'));
            }
        }
        return $this->render('EspritAppBackBundle:affectationsjoueurs:affectationsToMatchs.html.twig', array('form' => $form->createView()));
    }

    public function affectationsToFormationsAction() {
        $formauser = new FormaUser();
        $form = $this->createForm(new FormaUserType, $formauser);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $formauser = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($formauser);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été ajouté avec succés!');
                return $this->redirect($this->generateUrl('affectationsToFormations'));
            }
        }
        return $this->render('EspritAppBackBundle:affectationsjoueurs:affectationsToFormations.html.twig', array('form' => $form->createView()));
    }

    public function affectationsToCompetitionsAction() {
        $CompUser = new CompUser();
        $form = $this->createForm(new CompUserType, $CompUser);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $CompUser = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($CompUser);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été ajouté avec succés!');
                return $this->redirect($this->generateUrl('affectationsToCompetitions'));
            }
        }
        return $this->render('EspritAppBackBundle:affectationsjoueurs:affectationsToCompetitions.html.twig', array('form' => $form->createView()));
    }

}
