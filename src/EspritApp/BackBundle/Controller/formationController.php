<?php

namespace EspritApp\BackBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EspritApp\BackBundle\Entity\Formation;
use EspritApp\BackBundle\Entity\FormaUser;
use EspritApp\BackBundle\Form\FormationType;

class formationController extends Controller {

    public function addAction() {
        $formation = new Formation();
        $form = $this->createForm(new FormationType, $formation);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $formation = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($formation);
                $em->flush();
                return $this->redirect($this->generateUrl('formation_show'));
            }
        }
        return $this->render('EspritAppBackBundle:formations:add.html.twig', array('form' => $form->createView()));
    }

    public function updateAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $formation = $em->getRepository('EspritAppBackBundle:Formation')->findOneBy(array('id' => $id));
        if (!$formation) {
            throw $this->createNotFoundException('no   formation found');
        }

        $form = $this->createForm(new FormationType, $formation);
        if ($request->isMethod('Post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $formation = $form->getData();
                $em->persist($formation);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été modifiées avec succés!');
                return $this->redirect($this->generateUrl('formation_update', array('id' => $id)));
            }
        }
        return $this->render('EspritAppBackBundle:formations:update.html.twig', array('form' => $form->createView(), 'id' => $id));
    }

    public function showAction() {
        $em = $this->getDoctrine()->getManager();
        $formations = $em->getRepository('EspritAppBackBundle:Formation')->findAll();
        return $this->render('EspritAppBackBundle:formations:show.html.twig', array(
                    'formations' => $formations,
        ));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $formation = $em->getRepository('EspritAppBackBundle:Formation')->find($id);
        if (!$formation) {
            throw $this->createNotFoundException('No  formation found for id ' . $id);
        }
        $em->remove($formation);
        $em->flush();
        return $this->redirect($this->generateUrl('formation_show'));
    }

    public function affectFormationToJoueurAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $formations = $em->getRepository('EspritAppBackBundle:Formation')->findAll();
       $formationsUser = $em->getRepository('EspritAppBackBundle:FormaUser')->findAll();



        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }

        return $this->render('EspritAppFrontBundle:Formation:affectFormationToJoueur.html.twig', array(
                    'formations' => $formations,
                       'formationusers' => $formationsUser, 'formNewsLetters' => $formNewsLetters->createView()
        ));
    }

    public function affectFormationToArbitreAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $formations = $em->getRepository('EspritAppBackBundle:Formation')->findAll();
        $formationsUser = $em->getRepository('EspritAppBackBundle:FormaUser')->findAll();




        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }

        return $this->render('EspritAppFrontBundle:Formation:affectFormationToArbitre.html.twig', array(
                    'formations' => $formations,
                    'formationusers' => $formationsUser, 'formNewsLetters' => $formNewsLetters->createView()
        ));
    }

    public function affectFormationToJoueurByIdAction($id, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $formation = $em->getRepository('EspritAppBackBundle:Formation')->find($id);
        if (!$formation) {
            throw $this->createNotFoundException('No  formation found for id ' . $id);
        }
        $user = $this->getUser(); 
        $user->setIdFormation($formation->getId());
        $em->persist($user);
 
        $FormUser = new FormaUser;
        $FormUser->setIdUser($user);
        $FormUser->setIdFormation($formation);
        $em->persist($FormUser);
        $em->flush();



        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }
        return $this->redirect($this->generateUrl('affectFormationToJoueur'));
    }

    public function affectFormationToArbitreByIdAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $formation = $em->getRepository('EspritAppBackBundle:Formation')->find($id);
        if (!$formation) {
            throw $this->createNotFoundException('No  formation found for id ' . $id);
        }
        $user = $this->getUser();
        $user->setIdFormation($formation->getId());
        $em->persist($user);
        $FormUser = new FormaUser;
        $FormUser->setIdUser($user);
        $FormUser->setIdFormation($formation);
        $em->persist($FormUser);
        $em->flush();


        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('affectFormationToArbitre'));
    }

}
