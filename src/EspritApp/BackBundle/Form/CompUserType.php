<?php

namespace EspritApp\BackBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use FOS\UserBundle\Util\LegacyFormHelper;

class CompUserType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
               ->add('idCompetition', 'entity', array('class' => 'EspritApp\BackBundle\Entity\Competition',
                    'property' => 'nom',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => true
                ))
                ->add('idUser', 'entity', array('class' => 'EspritApp\BackBundle\Entity\Utilisateur',
                    'property' => 'nom',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => true
                ))

        ;
    }

    public function __construct(array $options = array()) {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
        $this->options = $resolver->resolve($options);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'EspritApp\BackBundle\Entity\CompUser'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'espritapp_backbundle_competitionuser';
    }

}
