<?php

namespace EspritApp\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use FOS\UserBundle\Util\LegacyFormHelper;

class TicketType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('prix')
                ->add('quantite')
                ->add('categorie')
                ->add('idMatche', 'entity', array('class' => 'EspritApp\BackBundle\Entity\Matche',
                    'property' => 'type',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => true
                ))

        ;
    }

    public function __construct(array $options = array()) {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
        $this->options = $resolver->resolve($options);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'EspritApp\BackBundle\Entity\Ticket'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'espritapp_backbundle_ticket';
    }

}
