<?php

namespace EspritApp\BackBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use FOS\UserBundle\Util\LegacyFormHelper;

class FormaUserType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
               ->add('idFormation', 'entity', array('class' => 'EspritApp\BackBundle\Entity\Formation',
                    'property' => 'nom',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => true
                ))
                ->add('idUser', 'entity', array('class' => 'EspritApp\BackBundle\Entity\Utilisateur',
                    'property' => 'nom',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => true
                ))

        ;
    }

    public function __construct(array $options = array()) {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
        $this->options = $resolver->resolve($options);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'EspritApp\BackBundle\Entity\FormaUser'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'espritapp_backbundle_formatuser';
    }

}
