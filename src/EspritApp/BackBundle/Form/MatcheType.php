<?php

namespace EspritApp\BackBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Doctrine\ORM\EntityManager;
use EspritApp\BackBundle\Entity\Repository\UtilisateurRepository;

class MatcheType extends AbstractType {

        private $em;
//    private $nom;  
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $this->em = $options['em'];
//        $this->nom = $options['nom'];


        $builder
                ->add('type', 'choice', array(
                    'choices' => array(
                        'International' => 'International',
                        'National' => 'National'
                    ),
                    'expanded' => false,
                    'multiple' => false
                ))
                ->add('dateDebut', 'date', array('widget' => 'single_text'))
                ->add('dateFin', 'date', array('widget' => 'single_text'))
                ->add('lieux')
                ->add('idEvent', 'entity', array('class' => 'EspritApp\BackBundle\Entity\Evenement',
                    'property' => 'nom',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false
                ))
                ->add('idCompetition', 'entity', array('class' => 'EspritApp\BackBundle\Entity\Competition',
                    'property' => 'nom',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false
                ))
              /*    ->add('arbitre',  'entity', array(
                    'class' => 'EspritAppBackBundle:Utilisateur',
                    'query_builder' => function (UtilisateurRepository $er)   {
                        return $er->findArbitres();
                    },
                    'property' => 'nom',
                    'multiple' => false,
                    'required' => true
                ))
                ->add('premierjoueur', 'entity', array(
                    'class' => 'EspritAppBackBundle:Utilisateur',
                    'query_builder' => function (UtilisateurRepository $er) {
                        return $er->findJoueurs();
                    },
                    'property' => 'nom',
                    'multiple' => false,
                    'required' => true
                ))
                ->add('secondjoueur', 'entity', array(
                    'class' => 'EspritAppBackBundle:Utilisateur',
                    'query_builder' => function (UtilisateurRepository $er)  {
                        return $er->findJoueurs();
                    },
                    'property' => 'nom',
                    'multiple' => false,
                    'required' => true
                ))*/
        ;
    }

 

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EspritApp\BackBundle\Entity\Matche',
             'em' => null 
//            'nom' => null,
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'espritapp_backbundle_matche';
    }

}
