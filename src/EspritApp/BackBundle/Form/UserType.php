<?php

namespace EspritApp\BackBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use FOS\UserBundle\Util\LegacyFormHelper;

class UserType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom')
                ->add('prenom')
                ->add('cin')
                ->add('adresse')
                ->add('telephone')
//                ->add('grade', 'text', array('required' => FALSE))
                ->add('etat', 'checkbox', array('required' => FALSE, 'data' => TRUE))
                ->add('image', 'file', array('required' => true, 'data_class' => null))
                ->add('description', 'text', array('required' => FALSE))
//                ->add('trancheAge')
                ->add('username')
                ->add('idFormation', 'entity', array('class' => 'EspritApp\BackBundle\Entity\Formation',
                    'property' => 'nom',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false
                ))
                ->add('idClub', 'entity', array('class' => 'EspritApp\BackBundle\Entity\Club',
                    'property' => 'nom',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false
                ))
                ->add('grade', 'choice', array(
                    'choices' => array(
                        'International' => 'International',
                        'National' => 'National',
                        'Amateur' => 'Amateur'
                    ),
                    'expanded' => false,
                    'multiple' => false
                ))
                ->add('trancheAge', 'choice', array(
                    'choices' => array(
                        'Senior' => 'Senior',
                        'Junior' => 'Junior',
                        'Veterant' => 'Cadet',
                      
                    ),
                    'expanded' => false,
                    'multiple' => false
                ))
                ->add('email')
                ->add('plainPassword', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\RepeatedType'), array(
                    'type' => LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'),
                    'options' => array('translation_domain' => 'FOSUserBundle'),
                    'first_options' => array('label' => 'form.password'),
                    'second_options' => array('label' => 'form.password_confirmation'),
                    'invalid_message' => 'fos_user.password.mismatch',
                ))
        ;
    }

    public function __construct(array $options = array()) {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
        $this->options = $resolver->resolve($options);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'EspritApp\BackBundle\Entity\Utilisateur'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'espritapp_backbundle_utilisateur';
    }

}
