<?php

namespace EspritApp\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EspritApp\BackBundle\Entity\Competition;
use EspritApp\BackBundle\Form\CompetitionType;

class competitionController extends Controller {

    public function showAction() {
        $em = $this->getDoctrine()->getManager();
        $competitions = $em->getRepository('EspritAppBackBundle:Competition')->findAll();
        return $this->render('EspritAppBackBundle:competitions:show.html.twig', array(
                    'competitions' => $competitions,
        ));
    }

    public function showfrontAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }


        $competitions = $em->getRepository('EspritAppBackBundle:Competition')->findAll();
        return $this->render('EspritAppFrontBundle:Competition:showfront.html.twig', array(
                    'competitions' => $competitions, 'formNewsLetters' => $formNewsLetters->createView()
        ));
    }

    public function addAction() {
        $Competition = new Competition();
        $form = $this->createForm(new CompetitionType, $Competition);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $Competition = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($Competition);
                $em->flush();

                // envoi mail notification pôur user abonnées
                $abonnées = $em->getRepository('EspritAppBackBundle:Utilisateur')->findBy(array('newsletter' => true));
                foreach ($abonnées as $key => $value) {
                    $mailer = $this->container->get('mailer');
                    $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                            ->setUsername('CGATunisieRep@gmail.com') ///  a completer
                            ->setPassword('CGATunisieRepCGATunisieRep'); ///  a completer
                    $mailer = \Swift_Mailer::newInstance($transport);
                    $message = \Swift_Message::newInstance('Test')
                            ->setSubject('Evenement')
                            ->setFrom('CGATunisieRep@gmail.comn') ///  a completer
                            ->setTo($value->getEmail()) /// $agent->getMailAdresse();
                            ->setBody("il ya des nouveaux competitions");
                    $result = $mailer->send($message);
                }
                return $this->redirect($this->generateUrl('competition_show'));
            }
        }
        return $this->render('EspritAppBackBundle:competitions:add.html.twig', array('form' => $form->createView()));
    }
 public function updateAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $Competition = $em->getRepository('EspritAppBackBundle:Competition')->findOneBy(array('id' => $id));
        if (!$Competition) {
            throw $this->createNotFoundException('no   Competition found');
        }
        $form = $this->createForm(new CompetitionType, $Competition);
        if ($request->isMethod('Post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $event = $form->getData();
                $em->persist($event);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été modifiées avec succés!');
                return $this->redirect($this->generateUrl('competition_update', array('id' => $id)));
            }
        }
        return $this->render('EspritAppBackBundle:competitions:update.html.twig', array('form' => $form->createView(), 'id' => $id));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $Competition = $em->getRepository('EspritAppBackBundle:Competition')->find($id);
        if (!$Competition) {
            throw $this->createNotFoundException('No   Competition found for id ' . $id);
        }
        $em->remove($Competition);
        $em->flush();
        return $this->redirect($this->generateUrl('competition_show'));
    }

}
