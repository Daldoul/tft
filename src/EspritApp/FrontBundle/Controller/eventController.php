<?php

namespace EspritApp\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use EspritApp\BackBundle\Entity\Evenement;
use EspritApp\BackBundle\Form\EventType;

class eventController extends Controller {

    public function showAction() {
        $em = $this->getDoctrine()->getManager();
        $events = $em->getRepository('EspritAppBackBundle:Evenement')->findAll();
        return $this->render('EspritAppBackBundle:events:show.html.twig', array(
                    'events' => $events,
        ));
    }

    public function showfrontAction(Request $request) {
//        $em = $this->getDoctrine()->getManager();
//        $events = $em->getRepository('EspritAppBackBundle:Evenement')->findAll();
//        return $this->render('EspritAppFrontBundle:Events:showfront.html.twig', array(
//                    'events' => $events,
//        ));
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT a FROM EspritAppBackBundle:Evenement a";
        $query = $em->createQuery($dql);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $query, /* query NOT result */ $request->query->getInt('page', 1)/* page number */, 2/* limit per page */
        );

        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }


        return $this->render('EspritAppFrontBundle:Events:showfront.html.twig', array('pagination' => $pagination, 'formNewsLetters' => $formNewsLetters->createView()));
    }

    public function addAction() {
        $event = new Evenement();
        $form = $this->createForm(new EventType, $event);
        $request = $this->getRequest();
        if ($request->isMethod('Post')) {
            $form->bind($request);
            if ($form->isValid()) {
                $event = $form->getData();
                $em = $this->getDoctrine()->getManager();
                $em->persist($event);
                $em->flush();

                // envoi mail notification pôur user abonnées
                $abonnées = $em->getRepository('EspritAppBackBundle:Utilisateur')->findBy(array('newsletter' => true));
                foreach ($abonnées as $key => $value) {
                    $mailer = $this->container->get('mailer');
                    $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                            ->setUsername('CGATunisieRep@gmail.com') ///  a completer
                            ->setPassword('CGATunisieRepCGATunisieRep'); ///  a completer
                    $mailer = \Swift_Mailer::newInstance($transport);
                    $message = \Swift_Message::newInstance('Test')
                            ->setSubject('Evenement')
                            ->setFrom('CGATunisieRep@gmail.comn')
                            ->setTo($value->getEmail()) /// $agent->getMailAdresse();
                            ->setBody("il ya des nouveaux evennements");
                    $result = $mailer->send($message);
                }

                return $this->redirect($this->generateUrl('event_show'));
            }
        }
        return $this->render('EspritAppBackBundle:events:add.html.twig', array('form' => $form->createView()));
    }

    public function updateAction($id, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository('EspritAppBackBundle:Evenement')->findOneBy(array('id' => $id));
        if (!$event) {
            throw $this->createNotFoundException('no   event found');
        }
        $form = $this->createForm(new EventType, $event);
        if ($request->isMethod('Post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $event = $form->getData();
                $em->persist($event);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été modifiées avec succés!');
                return $this->redirect($this->generateUrl('event_update', array('id' => $id)));
            }
        }
        return $this->render('EspritAppBackBundle:events:update.html.twig', array('form' => $form->createView(), 'id' => $id));
    }

    public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository('EspritAppBackBundle:Evenement')->find($id);
        if (!$event) {
            throw $this->createNotFoundException('No  event found for id ' . $id);
        }
        $em->remove($event);
        $em->flush();
        return $this->redirect($this->generateUrl('event_show'));
    }

}
