<?php

namespace EspritApp\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
//use EspritApp\BackBundle\Entity\Utilisateur;
use EspritApp\FrontBundle\Form\UserType;

class userController extends Controller {

    public function updateAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('id' => $id));
        if (!$user) {
            throw $this->createNotFoundException('no  user found');
        }
        $form = $this->createForm(new UserType, $user);
        if ($request->isMethod('Post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $user = $form->getData();
                $em->persist($user);
                $em->flush();
                $this->addFlash('notice', 'paramétres ont été modifiées avec succés!');
                return $this->redirect($this->generateUrl('user_update', array('id' => $id)));
            }
        }

        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }

        return $this->render('EspritAppFrontBundle:Profil:update.html.twig', array('form' => $form->createView(), 'formNewsLetters' => $formNewsLetters->createView(), 'id' => $id));
    }

}
