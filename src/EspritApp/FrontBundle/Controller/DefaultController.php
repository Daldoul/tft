<?php

namespace EspritApp\FrontBundle\Controller;

use RC\AmChartsBundle\AmCharts\AmPieChart;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    public function homeAction(Request $request) {

//         $mailer = $this->container->get('mailer');
//        $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com',465,'ssl')
//            ->setUsername('CGATunisieRep@gmail.com') ///  a completer 
//            ->setPassword('CGATunisieRepCGATunisieRep');///  a completer 
//        $mailer = \Swift_Mailer::newInstance($transport);
//        $message = \Swift_Message::newInstance('Test')
//                ->setSubject('Reception des fichiers')
//                ->setFrom('CGATunisieRep@gmail.comn')
//                ->setTo('lamehrez@yahoo.fr') /// $agent->getMailAdresse();
//                ->setBody("dsg");
//        $result = $mailer->send($message);



        $em = $this->getDoctrine()->getManager();
 
        $formations = $em->getRepository('EspritAppBackBundle:Formation')->findAll();
        $formation = array();
        if ($formations) {
            $formation[] = end($formations);
        }
        $matches = $em->getRepository('EspritAppBackBundle:Matche')->findAll();
        $matche = array();
        if ($matches) {
            $matche[] = end($matches);
        }

        $events = $em->getRepository('EspritAppBackBundle:Evenement')->findAll();
        $event = array();
        if ($events) {
            $event[] = end($events);
        }
        $competitions = $em->getRepository('EspritAppBackBundle:Competition')->findAll();
        $competition = array();
        if ($competitions) {
            $competition[] = end($competitions);
        }


        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }




        return $this->render('EspritAppFrontBundle:Default:home.html.twig', array(
                    'formation' => $formation, 'matchs' => $matche, 'event' => $event, 'competition' => $competition,
                    'formNewsLetters' => $formNewsLetters->createView()
        ));
    }

    public function statistiqueAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $matcheJoueurs = $em->getRepository('EspritAppBackBundle:MatcheUser')->findAll();


        $pieChart = new AmPieChart();
        $pieChart->renderTo('piechart');
        $pieChart->setTitleField('number');
        $pieChart->setValueField('column-1');

        foreach ($matcheJoueurs as $key => $value) {
            $arrayJoueur[$value->getIdUser()->getId()] = count($em->getRepository('EspritAppBackBundle:MatcheUser')->findBy(array('idUser' => $value->getIdUser()->getId())));
        }

        foreach ($arrayJoueur as $key => $value) {
            $pieChart->addData(array('number' => $key, 'column-1' => $value));
        }

        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }



        return $this->render('EspritAppFrontBundle:Statistiques:show.html.twig', array(
                    'chart' => $pieChart, 'formNewsLetters' => $formNewsLetters->createView()
        ));
    }

    public function showusersAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('EspritAppBackBundle:Utilisateur')->findAll();

        $arbitre = array();
        foreach ($users as $user) {
            if (in_array('ROLE_ARBITRE', $user->getRoles())) {
                $arbitre[] = $user;
            }
        }
        $client = array();
        foreach ($users as $user) {
            if (in_array('ROLE_CLIENT', $user->getRoles())) {
                $client[] = $user;
            }
        }
        $joueur = array();
        foreach ($users as $user) {
            if (in_array('ROLE_JOUEUR', $user->getRoles())) {
                $joueur[] = $user;
            }
        }
        $medecin = array();
        foreach ($users as $user) {
            if (in_array('ROLE_MEDECIN', $user->getRoles())) {
                $medecin[] = $user;
            }
        }
        $responsable = array();
        foreach ($users as $user) {
            if (in_array('ROLE_RESPONSABLE', $user->getRoles())) {
                $responsable[] = $user;
            }
        }
        $pieChart = new AmPieChart();
        $pieChart->renderTo('piechart');
        $pieChart->setTitleField('number');
        $pieChart->setValueField('column-1');


        $pieChart->renderTo('piechart');
        $pieChart->setTitleField('number');
        $pieChart->setValueField('column-1');
        $pieChart->addData(array('number' => '1', 'column-1' => count($arbitre)));
        $pieChart->addData(array('number' => '2', 'column-1' => count($client)));
        $pieChart->addData(array('number' => '3', 'column-1' => count($joueur)));
        $pieChart->addData(array('number' => '4', 'column-1' => count($medecin)));
        $pieChart->addData(array('number' => '5', 'column-1' => count($responsable)));

        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }

        return $this->render('EspritAppFrontBundle:Statistiques:showusers.html.twig', array(
                    'chart' => $pieChart, 'formNewsLetters' => $formNewsLetters->createView()
        ));
    }

    public function contactAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }
        return $this->render('EspritAppFrontBundle:Default:contact.html.twig', array(
                    'formNewsLetters' => $formNewsLetters->createView()
        ));
    }

    public function newsAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }
        return $this->render('EspritAppFrontBundle:Default:news.html.twig', array(
                    'formNewsLetters' => $formNewsLetters->createView()
        ));
    }

    public function playersAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $formNewsLetters = $this->createFormBuilder()
                ->add('username', 'text')
                ->add('mail', 'text')
                ->getForm();
        $formNewsLetters->handleRequest($request);
        if ($formNewsLetters->isValid()) {
            $username = $formNewsLetters["username"]->getData();
            $email = $formNewsLetters["mail"]->getData();
            $user = $em->getRepository('EspritAppBackBundle:Utilisateur')->findOneBy(array('email' => $email, 'username' => $username));
            if ($user) {
                $user->setNewsletter(true);
                $em->persist($user);
                $em->flush();
            }
        }
        return $this->render('EspritAppFrontBundle:Default:players.html.twig', array(
                    'formNewsLetters' => $formNewsLetters->createView()
        ));
    }

}
